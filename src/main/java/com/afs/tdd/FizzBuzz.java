package com.afs.tdd;

public class FizzBuzz {

    public String countOff(int i) {
        String result = "";
        if (i % 3 == 0) {
            result += "Fizz";
        }
        if (i % 5 == 0) {
            result += "Buzz";
        }
        if (i % 7 == 0) {
            result += "Whizz";
        }
        return result.isEmpty() ? String.valueOf(i) : result;
    }
}