package com.afs.tdd;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;

class FizzBuzzTest {

    @Test
    void should_return_1_when_count_off_given_order_number_1() {
        // given
        FizzBuzz fizzBuzz = new FizzBuzz();

        // when
        String actual = fizzBuzz.countOff(1);

        // then
        assertEquals("1", actual);
    }

    @Test
    void should_return_fizz_when_count_off_given_order_number_3() {
        // given
        FizzBuzz fizzBuzz = new FizzBuzz();

        // when
        String actual = fizzBuzz.countOff(3);

        // then
        assertEquals("Fizz", actual);
    }

    @Test
    void should_return_fizz_when_count_off_given_order_number_in_multiple_of_3() {
        // given
        FizzBuzz fizzBuzz = new FizzBuzz();

        // when
        assertAll(
            () -> assertEquals("Fizz", fizzBuzz.countOff(6)),
            () -> assertEquals("Fizz", fizzBuzz.countOff(9))
        );
    }

    @Test
    void should_return_buzz_when_count_off_given_order_number_in_multiple_of_5() {
        // given
        FizzBuzz fizzBuzz = new FizzBuzz();

        // when
        assertAll(
            () -> assertEquals("Buzz", fizzBuzz.countOff(5)),
            () -> assertEquals("Buzz", fizzBuzz.countOff(20))
        );
    }

    @Test
    void should_return_whizz_when_count_off_given_order_number_in_multiple_of_7() {
        // given
        FizzBuzz fizzBuzz = new FizzBuzz();

        // when
        assertAll(
            () -> assertEquals("Whizz", fizzBuzz.countOff(7)),
            () -> assertEquals("Whizz", fizzBuzz.countOff(14))
        );
    }

    @Test
    void should_return_fizzbuzz_when_count_off_given_order_number_in_common_multiple_of_3_and_5() {
        // given
        FizzBuzz fizzBuzz = new FizzBuzz();

        // when
        assertAll(
            () -> assertEquals("FizzBuzz", fizzBuzz.countOff(15)),
            () -> assertEquals("FizzBuzz", fizzBuzz.countOff(30))
        );
    }

    @Test
    void should_return_fizzwhizz_when_count_off_given_order_number_in_common_multiple_of_3_and_7() {
        // given
        FizzBuzz fizzBuzz = new FizzBuzz();

        // when
        assertAll(
            () -> assertEquals("FizzWhizz", fizzBuzz.countOff(21)),
            () -> assertEquals("FizzWhizz", fizzBuzz.countOff(42))
        );
    }

    @Test
    void should_return_buzzwhizz_when_count_off_given_order_number_in_common_multiple_of_5_and_7() {
        // given
        FizzBuzz fizzBuzz = new FizzBuzz();

        // when
        assertAll(
            () -> assertEquals("BuzzWhizz", fizzBuzz.countOff(35)),
            () -> assertEquals("BuzzWhizz", fizzBuzz.countOff(70))
        );
    }

    @Test
    void should_return_fizzbuzzwhizz_when_count_off_given_order_number_in_common_multiple_of_3_and_5_and_7() {
        // given
        FizzBuzz fizzBuzz = new FizzBuzz();

        // when
        assertAll(
            () -> assertEquals("FizzBuzzWhizz", fizzBuzz.countOff(105)),
            () -> assertEquals("FizzBuzzWhizz", fizzBuzz.countOff(210))
        );
    }
}
